<!DOCTYPE html>
<html lang="en">

<head>
    <title>Electronic shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    {{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script> --}}
</head>

<body>
    <header>
        <a href="/" class="logo"><i class="fas fa-shopping-cart"></i>eshop</a>
        <nav>
            <ul class="nav-link">
                <li>
                    <a href="">all products</a>
                    <ul class="mega-dropdown">
                        <li class="row">
                            <ul class="mega-col">
                                <li><a href="/product">computers</a></li>
                                <li><a href="#">monitors</a></li>
                                <li><a href="#">projectors</a></li>
                                <li><a href="#">camera</a></li>
                            </ul>
                            <ul class="mega-col">
                                <li><a href="#">battery</a></li>
                                <li><a href="#">accessories</a></li>
                                <li><a href="#">printer</a></li>
                                <li><a href="#">server</a></li>
                            </ul>
                            <ul class="mega-col">
                                <li><a href="#">computers</a></li>
                                <li><a href="#">monitors</a></li>
                                <li><a href="#">projectors</a></li>
                                <li><a href="#">camera</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li><a href="#">service</a></li>
                <li><a href="#">promotion</a></li>
                <li><a href="#">special discount</a></li>
            </ul>
        </nav>
        <div class="icons">
            <i class="fas fa-bars" id="menu-bars"></i>
            <i class="fas fa-search" id="search-icon"></i>
            {{-- <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-shopping-cart"></a> --}}
        </div>

    </header>

    @yield('content')

    <footer>
        <div class="container">
            <div class="wrapper">
                <div class="footer-widget">
                    <a href="#" class="logo"><i class="fas fa-shopping-cart"></i>eshop</a>
                    <p class="desc">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium aut delectus reprehenderit
                        eaque. Fuga iure eius, animi culpa quo reprehenderit.
                    </p>
                    <ul class="socials">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-telegram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="footer-widget">
                    <h2>contact us</h2>
                    <ul class="links">
                        <li><a href="#">contact us</a>
                        </li>
                        <li><a href="#">products</a>
                        </li>
                        <li><a href="#">information</a>
                        </li>
                        <li><a href="#">follow us</a></li>
                    </ul>
                </div>
                <div class="footer-widget">
                    <h2>products</h2>
                    <ul class="links">
                        <li><a href="#">contact us</a>
                        </li>
                        <li><a href="#">products</a>
                        </li>
                        <li><a href="#">information</a>
                        </li>
                        <li><a href="#">follow us</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-widget">
                    <h2>information</h2>
                    <ul class="links">
                        <li><a href="#">contact us</a>
                        </li>
                        <li><a href="#">products</a>
                        </li>
                        <li><a href="#">information</a>
                        </li>
                        <li><a href="#">follow us</a></li>
                    </ul>
                </div>
                <div class="footer-widget">
                    <h2>follow us</h2>
                    <ul class="links">
                        <li><a href="#">contact us</a>
                        </li>
                        <li><a href="#">products</a>
                        </li>
                        <li><a href="#">information</a>
                        </li>
                        <li><a href="#">follow us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="copyright-wrapper">
                <p>designed and develop by<a href="" target="blank"> it team</a></p>
            </div>
        </div>
    </footer>
</body>
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>

</html>

