@extends('layouts.masteruser')
@section('content')
    <section class="home">
    </section>
    <section class="products" id="products">
        <div id="main" class="colum2 colum2-left-sidebar boxed">
            <div class="brand-item owl-loaded owl-drag">
                <h4 class="brand-title">filter by brand</h4>
                <div class="owl-stage-outer">
                    <div class="owl-stage">
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/apple-150x150.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/microsoft-150x150.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/acer-150x150.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/asus-150x150.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/dell-1-150x150.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/lenovo-1-1-150x150.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/vaio-150x150-1.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/TOSHIBA-150x150.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="brand-inner">
                                <a href="">
                                    <img src="{{ asset('image/categoryImage/hp-1-150x150.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <main id="content" class="site-main" role="main">
                <div class="box-container">
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt=""
                            class="img-overlay-image">
                        <div class="description">
                            CPU: AMD Razer<br>
                            RAM: 8GB<br>
                            STORAGE: 256GB<br>
                            DISPLAY: 14.0 Inch<br>
                            GRAPHIC: AMD Radeon<br>
                            OS: 10 or 11
                        </div>

                        <h3>iphone 13 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookair.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>apple watch 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookpro.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>samsung note 10</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iwatch mini 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iphone 14 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                </div>
                <div class="box-container">
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iphone 13 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookair.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>apple watch 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookpro.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>samsung note 10</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iwatch mini 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iphone 14 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                </div>
                <div class="box-container">
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iphone 13 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookair.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>apple watch 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/macbookpro.png') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>samsung note 10</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iwatch mini 13</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                    <div class="box">
                        <div class="warranty"></div>
                        <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
                        <div class="description">
                            CPU: AMD Razer <br>
                            RAM: 8GB <br>
                            STORAGE: 256GB <br>
                            DISPLAY: 14.0 Inch <br>
                            GRAPHIC: AMD Radeon <br>
                            OS: 10 or 11
                        </div>
                        <h3>iphone 14 pro max</h3>
                        <strong>$1500.23</strong>
                        <span>$15.23</span>
                        {{-- <a href="#" class="btn">add to cart</a> --}}
                    </div>
                </div>
            </main>
    </section>
@endsection
