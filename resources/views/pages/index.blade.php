@extends('layouts.masteruser')
@section('content')
{{--section slide animation --}}
<section class="home">
    <div class="home-slider swiper-container">
        <div class="wrapper swiper-wrapper">
            <div class="swiper-slide slide">
                <div class="content">
                    <span>Our special promotion</span>
                    <h3>iphone 13 pro max</h3>
                    <p>អ្វីដែលអ្នកគាំទ្រទូរស័ព្ទ iPhone បានទន្ទឹងរងចាំគឺបានមកដល់ហើយ…ដោយ Apple បានប្រកាសចេញទូរស័ព្ទ
                        iOS ជំនាន់ថ្មីរបស់ខ្លួនគឺ iPhone 13 Series ជាផ្លូវការណ៍កាលពីយប់មិញនេះ។
                    </p>
                    <a href="#" class="btn">order now</a>
                </div>
                <div class="image">
                    <img src="{{ asset('image/slide/13promax.png') }}" alt="">
                </div>
            </div>
            <div class="swiper-slide slide">
                <div class="content">
                    <span>Our special promotion</span>
                    <h3>oppo f21</h3>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earum excepturi possimus adipisci
                        pariatur, fugiat magnam quod ad sit, fugit temporibus praesentium dolorem, ex saepe!
                        Molestias odit consectetur necessitatibus architecto cupiditate.</p>
                    <a href="#" class="btn">order now</a>
                </div>
                <div class="image">
                    <img src="{{ asset('image/slide/oppoF21.png') }}" alt="">
                </div>
            </div>
            <div class="swiper-slide slide">
                <div class="content">
                    <span>Our special promotion</span>
                    <h3>samsung snote 10</h3>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earum excepturi possimus adipisci
                        pariatur, fugiat magnam quod ad sit, fugit temporibus praesentium dolorem, ex saepe!
                        Molestias odit consectetur necessitatibus architecto cupiditate.</p>
                    <a href="#" class="btn">order now</a>
                </div>
                <div class="image">
                    <img src="{{ asset('image/slide/13promax.png') }}" alt="">
                </div>
            </div>
        </div>
        {{-- <div class="swiper-pagination"></div> --}}
    </div>
</section>
{{-- end section slide animation --}}

{{-- section show products --}}
<section class="products" id="products">
    <h3 class="sub-heading">our products</h3>
    <h1 class="heading">popular products</h1>
    <div class="box-container">
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/applewatch.png') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>iphone 13 pro max</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/macbookair.png') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>apple watch 13</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>samsung note 10</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>iwatch mini 13</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/ThinkPad X1 11th.jpg') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>iphone 14 pro max</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
    </div>

    <h1 class="heading">new arrival</h1>
    <div class="box-container">
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>iphone 13 pro max</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/macbookair.png') }}" alt="">
            <div class="description">
                CPU: AMD Razer<br>
                RAM: 8GB<br>
                STORAGE: 256GB<br>
                DISPLAY: 14.0 Inch<br>
                GRAPHIC: AMD Radeon<br>
                OS: 10 or 11
            </div>
            <h3>apple watch 13</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/macbookpro.png') }}" alt="">
            <h3>samsung note 10</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
            <h3>iwatch mini 13</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
            <h3>iphone 14 pro max</h3>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
    </div>

    <h1 class="heading">best selling</h1>
    <div class="box-container">
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/applewatch.png') }}" alt="">
            <h3>iphone 13 pro max</h3>
            <strong>$1500.23</strong>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/macbookair.png') }}" alt="">
            <h3>apple watch 13</h3>
            <strong>$1500.23</strong>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Dell inspron 3881.jpg') }}" alt="">
            <h3>samsung note 10</h3>
            <strong>$1500.23</strong>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/macbookpro.png') }}" alt="">
            <h3>iwatch mini 13</h3>
            <strong>$1500.23</strong>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
        <div class="box">
            <div class="warranty"></div>
            <img src="{{ asset('image/product/Lenovo IdeaCentre AIO.jpg') }}" alt="">
            <h3>iphone 14 pro max</h3>
            <strong>$1500.23</strong>
            <span>$15.23</span>
            {{-- <a href="#" class="btn">add to cart</a> --}}
        </div>
    </div>
</section>
{{-- end section show products --}}
@endsection

